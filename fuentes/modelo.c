/*	Grua 

	C.A.D. 						Curso 2008-2009
 	
	Codigo base para la realización de las practicas de CAD

	modulo modelo.c
	Dibujo del modelo
=======================================================
	J.C. Torres 
	Dpto. Lenguajes y Sistemas Informticos
	(Univ. de Granada, SPAIN)

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details 
 http://www.gnu.org/copyleft/gpl.html

=======================================================
Queda prohibido cobrar canon por la copia de este software

*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>                   // Libreria de utilidades de OpenGL

#define __modelo__			// Hace que se asigne memoria a las variables globales

#include "grua.h"



int COLORGRUA=0;


/**	void initModel()

Inicializa el modelo y de las variables globales

**/

void initModel()
{
	/**
		Definicion de los colores usados.
	**/
	float colores[10][4]={{1,1,0,1.},{0.7,0.7,0.7,1},{1.0,0.3,0.3,1},
	   		   {0.7,0.6,0.2,1},{0.2,1.0,1.0,1},{1.0,0.86,0.3,1},
			   {0.4,1,0.4,1.},{1,0.6,1,1.},{0,0,1,1.},{1,1,1,1.}};
	int i,j;
	for(i=0;i<4;++i)
		for(j=0;j<10;++j)
			color[j][i]=colores[j][i];

	COLORGRUA=0;

	/** 

	Parametros iniciales de la camara

	**/
	view_rotx=-30.0;	// Angulos de rotacion 
	view_roty=-45.0;
	view_rotz=0.0;
	d=100.0;
	
	x_camara=20;		// Posicion de la camara
	y_camara=2;
	z_camara=20;

	VISTA_PERSPECTIVA=0;	// Flag perspectiva/paralela

	ventanaMundoParalela=200;	// Ventana para proyeccion paralela
	origenXVentanaMundoParalelo=0;	
	origenYVentanaMundoParalelo=0;

	//*******
	// INICIALIZACIÓN DE LOS PARÁMETROS DE LA GRÚA
	//*******	
	grua.angY = 0; // Ángulo de giro de la base
	grua.posCarro = 15; // Distancia del carro a la torre
	grua.lCuerda = 20; // Longitud de la cuerda
	grua.vangY = 0; // Velocidad a la que cambia el ángulo de giro del brazo
	grua.vposCarro = 0; // Velocidad a la que cambia la distancia del carro a la torre
	grua.vlCuerda = 0; // Velocidad a la que cambia la longitud de la cuerda
	//*******
	// INICIALIZACIÓN DE LOS PARÁMETROS DEL OPERARIO
	//*******	
	operario.angCabeza = 0; // Ángulo de giro de la cabeza. 0 mira al frente
	operario.angBrazoIzq = 0; // Ángulo de giro del brazo izquierdo. 0 forma 35º con la vertical
	operario.angBrazoDer = 0; // Ángulo de giro del brazo derecho. 0 forma 35º con la vertical
	operario.angPiernas = 0; // Ángulo de giro de las piernas. 0 en vertical
	operario.posBoca = 0; // Posición de la sonrisa. 0 boca recta
}

/**	void Dibuja( void )

Procedimiento de dibujo del modelo. Es llamado por glut cada vez que se debe redibujar.

**/

void Dibuja( void )
{
	static GLfloat pos[4] = {5.0, 5.0, 10.0, 0.0 };		// Posicion de la fuente de luz
	int i;

	glPushMatrix();		// Apila la transformacion geometrica actual

		glClearColor(0,0,0.6,1);	// Fija el color de fondo a azul
		
		glClear( GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT );	 // Inicializa el buffer de color

		transformacionVisualizacion();	 // Carga transformacion de visualizacion

		glLightfv( GL_LIGHT0, GL_POSITION, pos );	// Declaracion de luz. Colocada aqui esta fija en la escena
		
		//ejes(3);	// Dibuja los ejes
		
		// Dibuja el suelo
		glPushMatrix();
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[verde]);
			glTranslatef(0,-0.5,0);
			caja(200,0.5,200);
		glPopMatrix();
		
		creaGrua();
		
		/*
		Práctica 0
		glPushMatrix();
			
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[marron]);
			glTranslatef(-5,0,0);
			caja(4,4,4);
			
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[gris]);
			glTranslatef(-5,0,0);
			falsoCilindro(20,0.05);
			
			glTranslatef(-5,0,0);
			glPushMatrix();
			glTranslatef(0,8,0);
			creaGancho(10);
			glPopMatrix();
			
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[amarillo]);
			glTranslatef(-5,0,0);
			creaTorre(10,1,1,10);
			
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[azul]);
			glTranslatef(-5,0,0);
			creaEstructura(12,3,3,4);
			
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[rojo]);
			glTranslatef(-5,0,0);
			creaBrazo(14,1,14);

		glPopMatrix();*/

	glPopMatrix(); 		// Desapila la transformacion geometrica

	glutSwapBuffers();	// Intercambia el buffer de dibujo y visualizacion
}

/**	void idle()

Procedimiento de fondo. Es llamado por glut cuando no hay eventos pendientes.

**/
void idle()
{
	actualizarModelo(); // Actualizar los parámetros del modelo
	gluiPostRedisplay();			// Redibuja
	glutTimerFunc(30,idle,0);		// Vuelve a lanzar otro evento de redibujado en 30 ms
}

void creaGrua(){

	glPushMatrix();
		
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[gris]);
		caja(15,1,15); // Base
		
		glTranslatef(0,1,0);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[amarillo]);
		creaEstructura(30,3,3,10); // Mástil
		
		glTranslatef(0,30,0);
		caja(3,0.5,3); // Base inferior del sistema de giro
		glTranslatef(0,0.75,0);
	
		glPushMatrix();
			glRotatef(90,1,0,0);
			glutSolidTorus(0.25,1,30,40); // Anillo del sistema de giro
		glPopMatrix();
	
		glTranslatef(0,0.25,0);
		
		glRotatef(grua.angY,0,1,0); // ROTACIÓN DEL BRAZO
		
		caja(3,0.5,3); // Base superior del sistema de giro
		
		glTranslatef(0,0.5,0);
		
		creaEstructura(6,3,3,2); // Mástil superior
	
		glPushMatrix(); // Cabina + Operario
			glTranslatef(0.5,1,0);
			glRotatef(90,0,1,0);
			
			creaCabina(); // Cabina de control
			
			glTranslatef(-2.3,0,-0.75);
			glRotatef(-90,0,1,0);
			creaOperario(); // Operario
			
		glPopMatrix();
	
		glTranslatef(0,6,0);
		
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[amarillo]);
		
		caja(3,0.5,3); // Base del porta flechas
		
		glTranslatef(0,0.5,0);
		
		creaTorre(9,3,3,3); // Porta flechas
	
		glPushMatrix(); // Inicio Flecha
			
			glTranslatef(1.5,0,0);
			
			glPushMatrix(); // Flecha Parte 1
			
				glRotatef(270,0,0,1);
				creaBrazo(15,3,4);
			
			glPopMatrix();
			
			glPushMatrix(); // Flecha Parte 2
			
				glTranslatef(30,0,0);
				glRotatef(180,0,1,0);
				glRotatef(270,0,0,1);
				creaBrazo(15,3,4);
			
			glPopMatrix();
			
			glTranslatef(0,-0.05,0);
			
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[gris]);
			glPushMatrix(); // Cables carro
			
				glTranslatef(0,-0.25,0);
				
				glPushMatrix(); // Cable 1
					glTranslatef(0,0,0.3);
					glRotatef(270,0,0,1);
					falsoCilindro(29.5,0.05);
				glPopMatrix();
				
				glPushMatrix(); // Cable 2
					glTranslatef(0,0,-0.3);
					glRotatef(270,0,0,1);
					falsoCilindro(29.5,0.05);
				glPopMatrix();
				
				glPushMatrix(); // Cable 3
					glRotatef(270,0,0,1);
					falsoCilindro(grua.posCarro-0.3,0.05);
				glPopMatrix();
			
			glPopMatrix();
			
			glPushMatrix(); // Enganche cables punta
			
				glTranslatef(29.5,-0.5,0);
				glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[amarillo]);
				caja(1,0.5,2);
				
			glPopMatrix();
			
			glPushMatrix(); // Distribución + Carro + Elevación + Polea + Gancho
			
				glTranslatef(grua.posCarro,-0.5,0); //DISTRIBUCIÓN
				glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[rojo]);
				caja(4,0.5,3); // Carro
				
				// Cables de trabajo ELEVACIÓN
				glTranslatef(0,-grua.lCuerda,0);
				//Cable 1
				glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[gris]);
				glPushMatrix();
					glTranslatef(-0.3,0,0);
					falsoCilindro(grua.lCuerda,0.05);
				glPopMatrix();
				//Cable 2
				glPushMatrix();
					glTranslatef(0.3,0,0);
					falsoCilindro(grua.lCuerda,0.05);
				glPopMatrix();
				
				//Polea
				glTranslatef(0,-2,0);
				glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[amarillo]);
				caja(1.5,2,0.5);
				
				//Gancho
				glRotatef(180,0,1,0);
				glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[gris]);
				creaGancho(2);
			
			glPopMatrix(); // Fin Carro
		
		glPopMatrix(); // Fin Flecha
	
		glPushMatrix(); // Inicio Contraflecha
		
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[amarillo]);
			glTranslatef(-1.5,0,0);
			glRotatef(180,0,1,0);
			
			glPushMatrix(); // Contraflecha Parte 1
			
				glRotatef(270,0,0,1);
				creaBrazo(5.25,3,2);
			
			glPopMatrix();
			
			glPushMatrix(); // Contraflecha Parte 2
			
				glTranslatef(10.5,0,0);
				glRotatef(180,0,1,0);
				glRotatef(270,0,0,1);
				creaBrazo(5.25,3,2);
			
			glPopMatrix();
			
			glPushMatrix(); // Contrapeso
			
				glTranslatef(7.875,-3.05,0);
				glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[gris]);
				caja(5.25,3,3);
				creaTirantesContrapeso();
			
			glPopMatrix(); // Contrapeso
		
		glPopMatrix(); // Fin Contraflecha
	
		glTranslatef(0,9,0);
	
		glPushMatrix(); // Tirante de la flecha
		
			glRotatef(257.0091,0,0,1);
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[gris]);
			falsoCilindro(28.4788,0.05);
			
		glPopMatrix();
		
		glPushMatrix(); // Tirante de la contraflecha
		
			glRotatef(124.328,0,0,1);
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[gris]);
			falsoCilindro(11.3523,0.05);
			
		glPopMatrix();
		
	glPopMatrix();
}

void creaTirantesContrapeso()
{
	glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[amarillo]);
	// Verticales
	glPushMatrix();
		glTranslatef(0,-0.1,1.525);
		falsoCilindro(3.2,0.05);
	glPopMatrix();
	
	glPushMatrix();
		glTranslatef(1.3125,-0.1,1.525);
		falsoCilindro(3.2,0.05);
	glPopMatrix();
	
	glPushMatrix();
		glTranslatef(-1.3125,-0.1,1.525);
		falsoCilindro(3.2,0.05);
	glPopMatrix();
	
	glPushMatrix();
		glTranslatef(0,-0.1,-1.525);
		falsoCilindro(3.2,0.05);
	glPopMatrix();
	
	glPushMatrix();
		glTranslatef(1.3125,-0.1,-1.525);
		falsoCilindro(3.2,0.05);
	glPopMatrix();
	
	glPushMatrix();
		glTranslatef(-1.3125,-0.1,-1.525);
		falsoCilindro(3.2,0.05);
	glPopMatrix();
	
	//Horizontales
	glPushMatrix();
		glTranslatef(0,-0.025,0);
		glRotatef(90,1,0,0);
		glTranslatef(0,-1.6,0);
		falsoCilindro(3.2,0.05);
	glPopMatrix();
	
	glPushMatrix();
		glTranslatef(1.3125,-0.025,0);
		glRotatef(90,1,0,0);
		glTranslatef(0,-1.6,0);
		falsoCilindro(3.2,0.05);
	glPopMatrix();
	
	glPushMatrix();
		glTranslatef(-1.3125,-0.025,0);
		glRotatef(90,1,0,0);
		glTranslatef(0,-1.6,0);
		falsoCilindro(3.2,0.05);
	glPopMatrix();
}

void creaCabina()
{
	glPushMatrix();
		// Cuerpo
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[blanco]);
		caja(3.5,4.5,4);
		
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[celeste]);
		// Cristal superior delantero
		glPushMatrix();
			glTranslatef(0,2.3,2);
			caja(3.3,2.1,0.1);
		glPopMatrix();
		
		// Cristal inferior delantero
		glPushMatrix();
			glTranslatef(0,0.1,2);
			caja(3.3,2.1,0.1);
		glPopMatrix();
		
		// Cristal derecho
		glPushMatrix();
			glTranslatef(-1.75,0.9,0.65);
			glRotatef(270,0,1,0);
			caja(2.5,3.5,0.1);
		glPopMatrix();
		
		// Cristal izquierdo
		glPushMatrix();
			glTranslatef(1.75,0.9,0.65);
			glRotatef(90,0,1,0);
			caja(2.5,3.5,0.1);
		glPopMatrix();
		
		glTranslatef(0,0,-0.75);
		// Plataforma
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[amarillo]);
		glPushMatrix();
			glTranslatef(0,-0.1,0);
			caja(6.5,0.1,5.5);
		glPopMatrix();
		
		// Barandilla
		glPushMatrix(); // Delante izquierda
			glTranslatef(3.2,0,2.7);
			falsoCilindro(1.5,0.03);
		glPopMatrix();
		
		glPushMatrix();
			glTranslatef(3.2,0,0.9);
			falsoCilindro(1.5,0.03);
		glPopMatrix();
		
		glPushMatrix();
			glTranslatef(3.2,0,-0.9);
			falsoCilindro(1.5,0.03);
		glPopMatrix();
		
		glPushMatrix(); // Detrás izquierda
			glTranslatef(3.2,0,-2.7);
			falsoCilindro(1.5,0.03);
		glPopMatrix();
		
		glPushMatrix();
			glTranslatef(1.06,0,-2.7);
			falsoCilindro(1.5,0.03);
		glPopMatrix();
		
		glPushMatrix();
			glTranslatef(-1.06,0,-2.7);
			falsoCilindro(1.5,0.03);
		glPopMatrix();
		
		glPushMatrix(); // Detrás derecha
			glTranslatef(-3.2,0,-2.7);
			falsoCilindro(1.5,0.03);
		glPopMatrix();
		
		glPushMatrix();
			glTranslatef(-3.2,0,0.9);
			falsoCilindro(1.5,0.03);
		glPopMatrix();
		
		glPushMatrix();
			glTranslatef(-3.2,0,-0.9);
			falsoCilindro(1.5,0.03);
		glPopMatrix();
		
		glPushMatrix(); // Delante derecha
			glTranslatef(-3.2,0,2.7);
			falsoCilindro(1.5,0.03);
		glPopMatrix();
		
		// Pasamanos
		glPushMatrix();
			glTranslatef(3.2,1.5,0);
			glRotatef(90,1,0,0);
			glTranslatef(0,-2.725,0);
			falsoCilindro(5.45,0.03);
		glPopMatrix();
		
		glPushMatrix();
			glTranslatef(-3.2,1.5,0);
			glRotatef(90,1,0,0);
			glTranslatef(0,-2.725,0);
			falsoCilindro(5.45,0.03);
		glPopMatrix();
		
		glPushMatrix();
			glTranslatef(0,1.5,-2.7);
			glRotatef(90,0,0,1);
			glTranslatef(0,-3.225,0);
			falsoCilindro(6.45,0.03);
		glPopMatrix();
		
		glPushMatrix();
			glTranslatef(0,1.5,2.7);
			glRotatef(90,0,0,1);
			glTranslatef(0,-3.225,0);
			falsoCilindro(6.45,0.03);
		glPopMatrix();
		
	glPopMatrix();
}

void actualizarModelo()
{
	// Orientación
	grua.angY+=grua.vangY;
	if(grua.angY>360) grua.angY-=360;
	if(grua.angY<0) grua.angY+=360;

	// Distribución
	if((grua.vposCarro > 0 && grua.posCarro < 26.8) || (grua.vposCarro < 0 && grua.posCarro > 2.2))
		grua.posCarro+=grua.vposCarro;

	// Elevación
	if((grua.vlCuerda > 0 && grua.lCuerda < 33.5) || (grua.vlCuerda < 0 && grua.lCuerda > 0.5))
		grua.lCuerda+=grua.vlCuerda;
}

void creaOperario()
{
	glPushMatrix();
		
		glTranslatef(0,-0.7+(0.7*cos((operario.angPiernas*3.141592)/180)),0);
		//Zapatos + Pantalones
		glPushMatrix();
			glTranslatef(0,0.95,0);
			glRotatef(operario.angPiernas,1,0,0);
			glTranslatef(0,-0.95,0);
			//Zapatos
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[gris]);
			//Zapato izquierdo
			glPushMatrix();
				glTranslatef(0.32,0,0.105);
				caja(0.34,0.2,0.7);
			glPopMatrix();
			//Zapato derecho
			glPushMatrix();
				glTranslatef(-0.32,0,0.105);
				caja(0.34,0.2,0.7);
			glPopMatrix();
			glTranslatef(0,0.2,0);
			//Pantalones
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[azul]);
			//Centro pantalón
			glPushMatrix();
				glTranslatef(0,0.3,0);
				caja(0.3,0.7,0.49);
			glPopMatrix();
			//Pierna izquierda
			glPushMatrix();
				glTranslatef(0.32,0,0);
				caja(0.34,1,0.49);
			glPopMatrix();
			//Pierna derecha
			glPushMatrix();
				glTranslatef(-0.32,0,0);
				caja(0.34,1,0.49);
			glPopMatrix();
		glPopMatrix();
		
		glTranslatef(0,0.95,0);
		
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[rojo]);
		caja(1,1,0.5);//Torso
		
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[naranja]);
		glTranslatef(0,1,0);
		
		glPushMatrix(); // Brazos
			glTranslatef(0,-0.275,0);
			
			glPushMatrix(); // Brazo izquierdo
				glTranslatef(0.575,0,0);
				glRotatef(35+operario.angBrazoIzq,1,0,0);
				glTranslatef(0,-0.125,0.25);
				caja(0.15,0.25,1);
			glPopMatrix();
			
			glPushMatrix(); // Brazo derecho
				glTranslatef(-0.575,0,0);
				glRotatef(35+operario.angBrazoDer,1,0,0);
				glTranslatef(0,-0.125,0.25);
				caja(0.15,0.25,1);
			glPopMatrix();
	
		glPopMatrix();
		
		caja(0.15,0.15,0.15);//Cuello
		glTranslatef(0,0.15,0);
		glRotatef(operario.angCabeza,0,1,0);
		caja(0.5,0.5,0.5);//Cabeza
		
		//Casco
		glPushMatrix();
			glTranslatef(0,0.5,0);
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[gris]);
			caja(0.6,0.25,0.6);//Cuerpo
			glTranslatef(0,0,0.35);
			caja(0.5,0.1,0.1);//Visera
		glPopMatrix();
	
		//Boca
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[rojo]);
		glPushMatrix();
			glTranslatef(0,0.1,0.2625);
			caja(0.05,0.05,0.025);
			glTranslatef(0,operario.posBoca,0);
			glPushMatrix();
				glTranslatef(0.05,0,0);
				caja(0.05,0.05,0.025);
			glPopMatrix();
			glPushMatrix();
				glTranslatef(-0.05,0,0);
				caja(0.05,0.05,0.025);
			glPopMatrix();
		glPopMatrix();

		// Ojos
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,color[azul]);
		glTranslatef(0,0.3,0.2625);
		
		glPushMatrix(); // Ojo izquierdo
			glTranslatef(0.125,0,0);
			caja(0.1,0.1,0.025);
		glPopMatrix();
	
		glPushMatrix(); // Ojo derecho
			glTranslatef(-0.125,0,0);
			caja(0.1,0.1,0.025);
		glPopMatrix();
		
	glPopMatrix();
}


Primera práctica de la asignatura Diseño Asistido por Computador. Curso 2011 - 2012.

El proyecto consiste en una aplicación gráfica interactiva en 3D que representa una grúa torre con un operario subido a ella. Ambos modelos pueden animarse mediante el teclado (Ver controles en la documentación) y las palancas de la interfaz (sólo la grúa). Esta práctica se utilizó para estudiar los modelos 3D jerárquicos.

Desarrollado en el lenguaje de programación C usando la librería gráfica OpenGL.

El código para gestionar la ventana de visualización, el Makefile y las piezas básicas de la grúa, nos fueron dados por los profesores de la asignatura. El código de la aplicación y el resto de modelos fue escrito por Juan Antonio Fajardo Serrano.

Capturas de pantalla de la aplicación en funcionamiento:

![Version final Todo extendido 2.jpg](https://bitbucket.org/repo/eXb4L5/images/29398432-Version%20final%20Todo%20extendido%202.jpg)

![Version final 2.jpg](https://bitbucket.org/repo/eXb4L5/images/1797404097-Version%20final%202.jpg)

![Version final Todo recogido 2.jpg](https://bitbucket.org/repo/eXb4L5/images/414392928-Version%20final%20Todo%20recogido%202.jpg)

![Version final 5.jpg](https://bitbucket.org/repo/eXb4L5/images/3042616583-Version%20final%205.jpg)

![Operario descansando.jpg](https://bitbucket.org/repo/eXb4L5/images/785713461-Operario%20descansando.jpg)

![Operario feliz.jpg](https://bitbucket.org/repo/eXb4L5/images/4149317260-Operario%20feliz.jpg)

![Operario triste.jpg](https://bitbucket.org/repo/eXb4L5/images/594383461-Operario%20triste.jpg)

![Operario saludando.jpg](https://bitbucket.org/repo/eXb4L5/images/3456623566-Operario%20saludando.jpg)